<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 09:54
 */

namespace Training\Traits;


trait ArrayMapperTrait
{
    /**
     * @param array $dataArray
     */
    public function setValuesFromArray(array $dataArray)
    {
        $className = __CLASS__;
        foreach ($dataArray as $key => $value) {
            if(property_exists($className, $key)) {
                $this->$key = $value;
            }
        }
    }
}