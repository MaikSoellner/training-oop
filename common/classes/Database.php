<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 10:27
 */

namespace Training\Config;


class Database
{
    private $conn;
    private $host;
    private $user;
    private $password;
    private $dbName;
    private $pdo;
    private $query;
    private $results;
    private $count = 0;
    private $error = false;

    /**
     * database constructor.
     * @param $host
     * @param $user
     * @param $password
     * @param $dbName
     */
    public function __construct()
    {
        $this->conn = false;
        $this->host = $GLOBALS["servername"];
        $this->user = $GLOBALS["login"];
        $this->password = $GLOBALS["pass"];
        $this->dbName = $GLOBALS["db"];

        $this->connect();
    }

    /**
     * @return bool|\PDO
     */
    public function connect()
    {
        if(!$this->conn){
            try{
                $this->conn = new \PDO("mysql:host=".$this->host.";dbname=".$this->dbName.";charset=utf8", $this->user, $this->password);
                $this->pdo = $this->conn;
            }
            catch(\Exception $e){
                die('<pre>Error: ' . $e->getMessage() . '</pre>');
            }
            if (!$this->conn) {
                $this->status_fatal = true;
                $this->error = true;
                echo 'Connection failed - check database connection';
                die();
            } else {
                $this->status_fatal = false;
            }
        }
        return $this->conn;
    }

    /**
     * @return mixed
     */
    public function getPDO()
    {
        return $this->pdo;
    }

    /**
     * @return int
     */
    public function disconnect() {
        if ($this->conn) {
            $this->conn = null;
        }
        if ($this->pdo) {
            $this->pdo = null;
        }
        return true;
    }
}