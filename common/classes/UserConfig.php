<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 03.09.2018
 * Time: 10:06
 */

namespace Training\UserAccount;


class UserConfig
{
    protected const TABLE_NAME = 'user';

    protected const COLUMN_ID = 'id';
    protected const COLUMN_FIRSTNAME = 'firstname';
    protected const COLUMN_LASTNAME = 'lastname';
    protected const COLUMN_TEAM_CODE = 'team_code';
}