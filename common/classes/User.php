<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 09:54
 */

namespace Training\UserAccount;


use Training\Traits\ArrayMapperTrait;

class User
{
    use ArrayMapperTrait;

    private $id;
    private $firstname;
    private $lastname;
    private $short;
    private $mail;
    private $team_code;

    private $team_name;

    /**
     * User constructor.
     * @param array $dataArray
     */
    public function __construct(array $dataArray)
    {
        $this->setValuesFromArray($dataArray);
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    protected function setId($id){
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * @param mixed $short
     */
    public function setShort($short): void
    {
        $this->short = $short;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getTeamCode() : string
    {
        return $this->team_code;
    }

    /**
     * @param string $team_code
     */
    public function setTeamCode($team_code): void
    {
        $this->team_code = $team_code;
    }

    /**
     * @return mixed
     */
    public function getTeamName()
    {
        return $this->team_name;
    }

    /**
     * @param mixed $team_name
     */
    public function setTeamName($team_name): void
    {
        $this->team_name = $team_name;
    }

    /**
     * @return array
     */
    public function getAsArray() : array
    {
        $returnArray = [];
        foreach($this as $key => $val){
            $returnArray[$key] = $val;
        }
        return $returnArray;
    }
}