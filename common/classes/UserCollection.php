<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.10.2018
 * Time: 17:25
 */

namespace Training\UserAccount;


use Training\Interfaces\CollectionInterface;

class UserCollection implements CollectionInterface
{
    private $entityCollection = [];

    public function __construct(array $dataArray)
    {
        foreach($dataArray as $key => $subarray){
            $this->add($subarray);
        }
    }

    /**
     * @param array $subarray
     * @return bool
     */
    function add(array $subarray) : bool
    {
        if(!array_key_exists($subarray['id'], $this->entityCollection)){
            $this->entityCollection[$subarray['id']] = new User($subarray);
            return true;
        }
        return false;
    }

    function removeByKey($key)
    {
        if(array_key_exists($key, $this->entityCollection)){
            unset($this->entityCollection[$key]);
            return true;
        }
        return false;
    }

    function getByKey($key)
    {
        if(array_key_exists($key, $this->entityCollection)){
            return $this->entityCollection[$key];
        }
        return null;
    }

    function getAll()
    {
        // TODO: Implement getAll() method.
    }


}

