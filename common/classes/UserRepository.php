<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 09:56
 */

namespace Training\UserAccount;


use Training\Interfaces\RepositoryInterface;

class UserRepository extends UserConfig implements RepositoryInterface
{
    private $pdo;

    /**
     * UserRepository constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param int $id
     * @return array
     */
    function getById(int $id): array
    {
        $stmt = "SELECT * FROM " . self::TABLE_NAME . " WHERE " . self::COLUMN_ID . " = :" . self::COLUMN_ID;
        $stmtPrepared = $this->pdo->prepare($stmt);
        $stmtPrepared->bindParam(':' . self::COLUMN_ID , $id);

        try{
            $stmtPrepared->execute();
        }
        catch(\Exception $e){
            die('There was an error in function '.__FUNCTION__.' in class '.__CLASS__.' on line '.__LINE__.'. Message: ' . $e->getMessage());
        }

        $dataArray = $stmtPrepared->fetchAll(\PDO::FETCH_ASSOC);
        return $dataArray[0];
    }

    /**
     * @param int $id
     * @return array
     */
    function getByIdWithTeamName(int $id): array
    {
        $stmt = "SELECT user.*, team.name as team_name FROM " . self::TABLE_NAME . " 
                      left JOIN team 
                      ON team.code = user.team_code 
                      WHERE user." . self::COLUMN_ID . " = :" . self::COLUMN_ID;
        $stmtPrepared = $this->pdo->prepare($stmt);
        $stmtPrepared->bindParam(':' . self::COLUMN_ID , $id);
        try{
            $stmtPrepared->execute();
        }
        catch(\Exception $e){
            die('There was an error in function '.__FUNCTION__.' in class '.__CLASS__.' on line '.__LINE__.'. Message: ' . $e->getMessage());
        }
        $dataArray = $stmtPrepared->fetchAll(\PDO::FETCH_ASSOC);
        return $dataArray[0];
    }

    /**
     * @return array
     */
    function getAll(): array
    {
        $stmt = "SELECT * FROM " . self::TABLE_NAME;
        $stmtPrepared = $this->pdo->prepare($stmt);
        try{
            $stmtPrepared->execute();
        }
        catch(\Exception $e){
            die('There was an error in function '.__FUNCTION__.' in class '.__CLASS__.' on line '.__LINE__.'. Message: ' . $e->getMessage());
        }
        $dataArray = $stmtPrepared->fetchAll(\PDO::FETCH_ASSOC);
        return $dataArray;
    }

    function create($currentInstance): bool
    {
        // TODO: Implement create() method.
    }

    /**
     * @param User $currentInstance
     * @return bool
     */
    function edit($currentInstance): bool
    {
        $stmt = "UPDATE " . self::TABLE_NAME . " 
                    SET " . self::COLUMN_FIRSTNAME . " = :" . self::COLUMN_FIRSTNAME;
        $stmtPrepared = $this->pdo->prepare($stmt);
        $stmtPrepared->bindParam(':' . self::COLUMN_FIRSTNAME , $currentInstance->getFirstname());
        try{
            $stmtPrepared->execute();
        }
        catch(\Exception $e){
            die('There was an error in function '.__FUNCTION__.' in class '.__CLASS__.' on line '.__LINE__.'. Message: ' . $e->getMessage());
        }
        $dataArray = $stmtPrepared->fetchAll(\PDO::FETCH_ASSOC);
        return $dataArray;
    }

    function delete(int $id): bool
    {
        // TODO: Implement delete() method.
    }
}