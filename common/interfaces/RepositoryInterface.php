<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 09:53
 */

namespace Training\Interfaces;


interface RepositoryInterface
{
    /**
     * @param int $id
     * @return array
     */
    function getById(int $id) : array;

    /**
     * @return array
     */
    function getAll() : array;

    /**
     * @param $currentInstance
     * @return bool
     */
    function create($currentInstance) : bool;

    /**
     * @param $currentInstance
     * @return bool
     */
    function edit($currentInstance) : bool;

    /**
     * @param int $id
     * @return bool
     */
    function delete(int $id) : bool;
}