<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.10.2018
 * Time: 17:25
 */

namespace Training\Interfaces;


interface CollectionInterface
{
    function add(array $dataArray) : bool;

    function removeByKey($key);

    function getByKey($key);

    function getAll();
}