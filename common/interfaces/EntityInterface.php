<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 09:52
 */

namespace Training\Interfaces;


interface EntityInterface
{
    /**
     * @return int
     */
    function getId() : int;
}