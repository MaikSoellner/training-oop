<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 23.08.2018
 * Time: 09:50
 */

use \Training\Config\Database;
use Training\UserAccount\UserRepository;
use Training\UserAccount\User;
use Training\UserAccount\UserCollection;

ini_set('error_reporting', TRUE);

require_once 'config/database.php';
require_once 'autoloader.inc.php';
require_once 'vendor/mustache/src/Mustache/Autoloader.php';
Mustache_Autoloader::register();

$TemplatePath = 'templates/';
$Mustache = new \Mustache_Engine(array(
    'loader' => new \Mustache_Loader_FilesystemLoader($TemplatePath)
));

$userId = 33;

$database = new Database();
$PDO = $database->connect();

$UserRepository = new UserRepository($PDO);

$User = new User($UserRepository->getByIdWithTeamName($userId));

echo "<pre>";
var_dump($User);
echo "</pre>";

$User->setFirstname('Peter');

echo "<pre>";
var_dump($User);
echo "</pre>";

$UserCollection = new UserCollection($UserRepository->getAll());

$UserRepository->edit($User);

$UserDataArray = $User->getAsArray();
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Training OOP</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>

<?php

$UserDataArray['projects'] = [];
$UserDataArray['projects'][0]['name'] = 'Bach Optik';
$UserDataArray['projects'][1]['name'] = 'Reusch';
$UserDataArray['projects'][2]['name'] = 'Test';
$UserDataArray['projects'][3]['name'] = 'Neues';

echo "<pre>";
var_dump($UserDataArray);
echo "</pre>";

echo $Mustache->render('user_account', $UserDataArray);
?>

</body>
</html>