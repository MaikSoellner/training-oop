-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 23. Aug 2018 um 10:53
-- Server-Version: 5.5.61-MariaDB
-- PHP-Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `web2186_1`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `last_modified_date` int(20) NOT NULL,
  `last_modified_userid` int(20) NOT NULL,
  `direct_dialing` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `team`
--

INSERT INTO `team` (`id`, `code`, `name`, `last_modified_date`, `last_modified_userid`, `direct_dialing`) VALUES
(1, 'CUST-CARE', 'Customer Care', 0, 0, 30),
(2, 'FINANCE', 'Finance & Controlling', 0, 0, 10),
(3, 'HR', 'Human Resources & Office Mgt.', 0, 0, 10),
(4, 'INNOVATION', 'Innovation Lab', 0, 0, 40),
(5, 'KEY-ACC', 'Key Account & Customizing', 0, 0, 60),
(6, 'MGT-BOARD', 'Management Board', 0, 0, 10),
(7, 'OSM', 'Online-Marketing', 0, 0, 80),
(8, 'PROJECTS', 'E-Commerce Projects', 0, 0, 50),
(9, 'SALES', 'Sales', 0, 0, 20),
(10, 'WEBDESIGN', 'Content & Design', 0, 0, 70);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `short` varchar(5) CHARACTER SET utf8mb4 DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `mail` varchar(100) NOT NULL,
  `team_id` int(11) NOT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_lost_hash` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `password_lost_code` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `password_lost_timestamp` int(255) NOT NULL,
  `language` varchar(23) NOT NULL DEFAULT 'de',
  `rights` int(1) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  `current_login` date DEFAULT NULL,
  `unlocked` int(1) DEFAULT '0',
  `blocked` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(50) DEFAULT NULL,
  `cookie_accept` tinyint(2) NOT NULL DEFAULT '0',
  `lastaction` int(20) NOT NULL,
  `holiday_hours_total` float NOT NULL DEFAULT '200',
  `holiday_hours_taken` float NOT NULL DEFAULT '0',
  `direct_dialing` int(10) NOT NULL DEFAULT '0',
  `job_title` varchar(30) NOT NULL,
  `picture` blob NOT NULL,
  `team_code` varchar(10) NOT NULL,
  `employment_date` date NOT NULL,
  `birth_day` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `short`, `position`, `mail`, `team_id`, `gender`, `status`, `password`, `password_lost_hash`, `password_lost_code`, `password_lost_timestamp`, `language`, `rights`, `last_login`, `current_login`, `unlocked`, `blocked`, `session_id`, `cookie_accept`, `lastaction`, `holiday_hours_total`, `holiday_hours_taken`, `direct_dialing`, `job_title`, `picture`, `team_code`, `employment_date`, `birth_day`) VALUES
(22, 'Maik', 'Söllner', 'MS', '5', 'msoellner@dc-solution.de', 5, '2', '0', '$2y$10$RS.WdFY.1SCMYF/Di/KDzOI3mqAoAvUjUJ8aZy6ONRO7sLTX1nygO', '', '3409', 1514897338, 'de', 2, 1532958585, '0000-00-00', 0, 0, '5b683d5e49205', 1, 1533558112, 200, 60.5, 57, 'Trainee', '', 'PROJECTS', '2016-08-01', '1997-07-22'),
(33, 'David', 'Stöpel', 'DS', '7', 'stoepel@dc-solution.de', 5, '2', '0', '$2y$10$YEoyWFIm4uGncQKr1IFm2ez0dSB/wyJGi8bfkXnscduT6j6JQiZWC', '', '9376', 1514879821, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 59, 'Junior Consultant', '', 'PROJECTS', '2017-11-01', '1993-07-21'),
(34, 'Marcus', 'Hassa', 'MH', '9', 'hassa@dc-solution.de', 5, '2', '0', '$2y$10$6c2Y1wOv.Y4O87k2srY3WOeow7FHAvXmFCEeV.AQf.L4g8kOS4fG6', '', '6769', 1514991291, 'de', 0, 1514991400, NULL, 0, 0, NULL, 1, 0, 200, 0, 52, 'Senior Consultant', '', 'PROJECTS', '2015-03-01', '1985-09-13'),
(35, 'Max', 'Wagner', 'MW', '7', 'wagner@dc-solution.de', 5, '2', '0', '$2y$10$NS8Dsg3OvYSzkykHj2HrNOxaa0CNabM0wQ6HZsKNB9W0zeJ8wUuKm', '', '3423', 1514389579, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 53, 'Consultant', '', 'PROJECTS', '2013-09-01', '1994-07-07'),
(36, 'Oliver', 'Fehmel', 'OF', '7', 'fehmel@dc-solution.de', 5, '2', '0', '$2y$10$W9SbMC1w5ejZ0gT1OwePMefnrLLX41N/H7D77NYpR..I536mj4CwW', '', '6199', 1514880285, 'de', 0, 1531981810, NULL, 0, 0, 'prta68ttj0ja6rk6vq98a9tn17', 1, 1531981810, 200, 0, 55, 'Junior Consultant', '', 'PROJECTS', '2017-01-01', '1990-02-20'),
(37, 'Tino', 'Lerner', 'TIL', '12', 'lerner@dc-solution.de', 5, '2', '0', '$2y$10$avfdqEvs1YADpfB1YxM3wO8FJ1EkPdYAvaAoZsKeSeNjChvyXyl3u', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 240, 0, 51, 'Manager ', '', 'PROJECTS', '2013-08-15', '1978-07-27'),
(39, 'Christoph', 'Werner', 'CW', '12', 'werner@dc-solution.de', 3, '2', '0', '$2y$10$6Gaw4EiS.2.wJ0HH57/GruoFXsmU7tsNTUlhT5eYJtQ9/818lnL.K', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 31, 'Manager ', '', 'FINANCE', '2010-10-01', '1985-06-21'),
(40, 'Tobias', 'Langmeyer', 'TL', '15', 'langmeyer@dc-solution.de', 2, '2', '0', '$2y$10$4YKl/EjP4eErInIo1Jkk5uOpDCMnC.qo7S58qVgwp9oMNdAVVcVOm', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 12, 'Manager', '', 'SALES', '2010-07-01', '1984-07-26'),
(41, 'Jörg', 'Söllner', 'JS', '12', 'soellner@dc-solution.de', 6, '2', '0', '$2y$10$VtI4VnUFGGPl2xvUXDKt8.zSmXvJi9nka0zW9MziTPhFEs8H.YkGG', '', '0544', 1514389390, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 61, 'Manager', '', 'KEY-ACC', '2013-11-01', '1988-06-08'),
(42, 'Kristina', 'Barth', 'KB', '12', 'barth@dc-solution.de', 1, '1', '0', '', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 13, 'Manager', '', 'HR', '2016-10-01', '1990-02-24'),
(43, 'Valeska-Marie', 'Kröner', 'VK', '8', 'kroener@dc-solution.de', 2, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 160, 0, 21, 'Consultant', '', 'SALES', '2014-02-01', '1984-02-10'),
(44, 'Sebastian', 'Lorenz', 'SL', '12', 'lorenz@dc-solution.de', 4, '2', '0', '', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 41, 'Manager', '', 'INNOVATION', '2014-04-01', '1983-01-27'),
(45, 'Malte', 'Bröcker', 'MALB', '12', 'broecker@dc-solution.de', 7, '2', '0', '', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 71, 'Manager', '', 'WEBDESIGN', '2016-10-01', '1987-09-10'),
(46, 'Marcel', 'Fischer', 'MF', '12', 'fischer@dc-solution.de', 8, '2', '0', '', '', '', 0, 'de', 1, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 81, 'Manager ', '', 'OSM', '2015-01-01', '1977-11-14'),
(47, 'Ahmed', 'Alsotohy', 'AA', NULL, 'alsotohy@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 44, 'Consultant', '', 'INNOVATION', '2017-01-01', '1990-08-01'),
(48, 'Andreas', 'Helm', 'AH', NULL, 'helm@dc-solution.de', 0, '0', '1', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Partneragentur', '', '', '0000-00-00', '1987-04-08'),
(49, 'Anna-Lena', 'Kölbel', 'AK', NULL, 'koelbel@dc-solution.de', 0, '1', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 13, 'Assistenz der Geschäftsleitung', '', 'MGT-BOARD', '2015-10-01', '1988-01-04'),
(50, 'Angela', 'Bär', 'ANB', NULL, 'baer@dc-solution.de', 0, '1', '1', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 125, 0, 14, 'Office Managerin', '', 'HR', '2016-09-01', '1987-09-01'),
(51, 'Björn', 'Pscherer', 'BP', NULL, 'pscherer@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 34, 'Junior Consultant', '', 'CUST-CARE', '2016-09-01', '1980-05-08'),
(52, 'Celina', 'Endres', 'CE', NULL, 'endres@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 84, 'Trainee', '', 'OSM', '2016-09-01', '1997-03-14'),
(53, 'Corinna', 'Langmeyer', 'CL', NULL, 'clangmeyer@dc-solution.de', 0, '0', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 64, 'Assistenz der Geschäftsleitung', '', 'MGT-BOARD', '2016-02-22', '1988-03-29'),
(54, 'Dmytro', 'Kovaliuk', 'DK', NULL, 'Kovaliuk@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 82, 'Consultant', '', 'OSM', '2017-09-15', '1992-01-16'),
(55, 'Daniel', 'Maier', 'DM', NULL, '', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Freelancer', '', 'KEY-ACC', '2018-03-01', '1994-06-04'),
(56, 'Dominik', 'Kraus', 'DOK', NULL, 'kraus@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 75, 'Trainee', '', 'WEBDESIGN', '2018-01-01', '1995-01-30'),
(57, 'Darius', 'Rey', 'DR', NULL, 'rey@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 63, 'Consultant', '', 'KEY-ACC', '2018-01-01', '1996-07-16'),
(58, 'Felix', 'Pavli', 'FP', NULL, 'pavli@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 92, 'Aushilfe', '', '', '2012-09-01', '1994-04-12'),
(59, 'Fabian', 'Stratenhoff', 'FS', NULL, 'stratenhoff@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 224, 0, 54, 'Consultant ', '', 'PROJECTS', '2016-07-01', '1991-01-16'),
(60, 'Jennifer', 'Le', 'JL', NULL, 'le@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 87, 'Trainee', '', 'OSM', '2017-09-01', '1990-09-07'),
(61, 'Jonas', 'Kneipp', 'JOK', NULL, 'kneipp@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 54, 'Consultant', '', 'PROJECTS', '2018-04-01', '1987-08-13'),
(62, 'Jasper', 'Richter', 'JR', NULL, 'richter@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 23, 'Trainee', '', 'SALES', '2016-08-01', '1997-01-04'),
(63, 'Katharina', 'Nüsslein', 'KN', NULL, 'nuesslein@dc-solution.de', 0, '1', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 120, 0, 16, 'Finance Assistant', '', 'MGT-BOARD', '2017-10-01', '0000-00-00'),
(64, 'Luis', 'Balbach', 'LB', NULL, 'balbach@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 22, 'Junior Consultant ', '', 'SALES', '2017-02-01', '1994-09-20'),
(65, 'Lea', 'Scholz', 'LES', NULL, 'scholz@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 240, 0, 15, 'Office Managerin', '', 'HR', '2018-08-01', '0000-00-00'),
(66, 'Laura', 'Köhnlein', 'LK', NULL, 'koehnlein@dc-solution.de', 0, '1', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 15, 'Trainee', '', 'HR', '2017-08-01', '1996-04-19'),
(67, 'Lukas', 'Oppelt', 'LO', NULL, 'oppelt@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 80, 0, 0, 'Werkstudent', '', 'INNOVATION', '2018-05-01', '1991-12-09'),
(68, 'Luisa', 'Savenko', 'LS', NULL, 'savenko@dc-solution.de', 0, '1', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 100, 0, 75, 'Trainee', '', 'WEBDESIGN', '2017-08-15', '1991-06-20'),
(69, 'Martin', 'Banasiak', 'MAB', NULL, 'banasiak@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 73, 'Consultant', '', 'WEBDESIGN', '2015-09-01', '1989-09-23'),
(70, 'Marcel', 'Hasse', 'MAH', NULL, 'hasse@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Feelancer', '', 'PROJECTS', '0000-00-00', '1980-01-16'),
(71, 'Marcel', 'Schmid', 'MAS', NULL, 'schmid@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 74, 'Junior Consultant', '', 'WEBDESIGN', '2017-03-15', '1992-10-19'),
(72, 'Manuela', 'Wagner', 'MAW', NULL, 'wagner@argo-logistics.de', 0, '0', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Sachbearbeiterin', '', '', '2015-07-15', '1969-07-12'),
(73, 'Michael', 'Bauer', 'MB', NULL, 'bauer@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 150, 0, 42, 'Senior Consultant', '', 'INNOVATION', '2011-05-01', '1983-12-11'),
(74, 'Marc', 'Eichner', 'ME', NULL, 'eichner@dc-solution.de', 0, '0', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 72, 'Art Director', '', 'WEBDESIGN', '2016-01-28', '2016-09-30'),
(75, 'Manuela', 'Groß', 'MG', NULL, 'gross@dc-solution.de', 0, '1', '1', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 82, 'Consultant', '', 'OSM', '2015-07-01', '1990-05-28'),
(76, 'Michael', 'Ihnen', 'MI', NULL, 'mi@ihnen-group.de', 0, '0', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 11, 'Geschäftsführer', '', 'MGT-BOARD', '2000-06-01', '1982-05-24'),
(77, 'Mitja', 'Bingold', 'MIB', NULL, 'bingold@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 35, 'Trainee ', '', 'CUST-CARE', '2017-08-01', '1993-09-01'),
(78, 'Markus', 'Meyer', 'MM', NULL, 'meyer@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 77, 'Consultant', '', 'WEBDESIGN', '2018-04-01', '1983-10-20'),
(79, 'Moritz', 'Putz', 'MP', NULL, 'putz@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 86, 'Trainee ', '', 'OSM', '2017-08-01', '1999-05-02'),
(80, 'Michelle', 'Ramsauer', 'MR', NULL, 'ramsauer@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 79, 'Trainee', '', 'WEBDESIGN', '2018-08-01', '1998-03-29'),
(81, 'Nina', 'Schmidt', 'NIS', NULL, 'schmidt@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 85, 'Junior Consultant', '', 'OSM', '2017-01-01', '1991-05-24'),
(82, 'Patrick', 'Bär', 'PB', NULL, 'pbaer@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 45, 'Auszubildender', '', 'INNOVATION', '2018-08-01', '2000-02-07'),
(83, 'Patrick', 'Haderthauer', 'PH', NULL, 'haderthauer@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 33, 'Consultant', '', 'CUST-CARE', '2011-09-01', '1989-06-23'),
(84, 'Philipp', 'Nassau', 'PN', NULL, 'nassau@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 83, 'Junior Consultant', '', 'OSM', '2015-04-01', '1988-02-22'),
(85, 'Pascal', 'Pöhlmann', 'PP', NULL, 'poehlmann@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 45, 'Trainee ', '', 'INNOVATION', '2017-06-01', '1996-07-30'),
(86, 'Philipp', 'Werner', 'PW', NULL, 'pwerner@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 240, 0, 58, 'Manager', '', 'CUST-CARE', '2017-11-01', '1978-12-29'),
(87, 'Ralf', 'Neuber', 'RN', NULL, 'neuber@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 216, 0, 35, 'Consultant ', '', 'CUST-CARE', '2017-01-09', '1970-10-20'),
(88, 'Sergius', 'Wiebe', 'SEW', NULL, 'wiebe@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 67, 'Trainee', '', 'KEY-ACC', '2017-08-15', '1987-06-03'),
(89, 'Sandra', 'Flegler', 'SF', NULL, 'flegler@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 100, 0, 16, 'Finance Assistant', '', 'FINANCE', '2017-11-01', '1993-05-11'),
(90, 'Sebastian', 'Grosch', 'SG', NULL, 'grosch@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 62, 'Junior Consultant', '', 'KEY-ACC', '2016-04-01', '1990-01-21'),
(91, 'Steve', 'Haun', 'SH', NULL, 'haun@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 32, 'Consultant', '', 'CUST-CARE', '2010-09-22', '1981-11-06'),
(92, 'Stefan', 'Niebler', 'SN', NULL, 'niebler@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Trainee', '', 'PROJECTS', '2016-07-01', '1992-12-02'),
(93, 'Stefan', 'Goertz', 'STG', NULL, 'goertz@dc-solution.de', 0, '0', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Freelancer', '', 'OSM', '0000-00-00', '0000-00-00'),
(94, 'Stefan', 'Huberth', 'STH', NULL, 'huberth@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 76, 'Junior Consultant', '', 'WEBDESIGN', '2018-03-01', '1989-07-28'),
(95, 'Samantha', 'Wieszt', 'SW', NULL, '', 0, '0', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 0, 'Reinigungskraft', '', '', '0000-00-00', '0000-00-00'),
(96, 'Svenja', 'Zeitler', 'SZ', NULL, 'zeitler@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 80, 0, 78, 'Werkstudentin', '', 'WEBDESIGN', '2018-04-01', '1997-09-02'),
(97, 'Tomasz', 'Forstmeier', 'TB', NULL, 'forstmeier@dc-solution.de', 0, '0', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 43, 'Consultant', '', 'INNOVATION', '2013-12-01', '1984-11-19'),
(98, 'Timo', 'Heinold ', 'TH', NULL, 'heinold@ic-innovative.de', 0, '0', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 0, '', '', '', '0000-00-00', '0000-00-00'),
(99, 'Timo', 'Holland', 'TIH', NULL, 'holland@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 64, 'Consultant', '', 'KEY-ACC', '2018-07-01', '1991-01-12'),
(100, 'Tino', 'Koban', 'TK', NULL, 'koban@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 0, 0, 63, 'Junior Consultant', '', 'KEY-ACC', '2015-10-01', '1990-08-27'),
(101, 'Tim', 'Rettner', 'TR', NULL, 'rettner@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 80, 0, 56, 'Trainee', '', 'PROJECTS', '2016-03-01', '1988-06-22'),
(102, 'Ullrich', 'Schoberth', 'US', NULL, 'schoberth@dc-solution.de', 0, '2', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 66, 'Consultant', '', 'KEY-ACC', '2017-04-01', '1978-08-11'),
(103, 'Verena', 'Scherm', 'VS', NULL, 'scherm@dc-solution.de', 0, '1', '0', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 65, 'Trainee ', '', 'KEY-ACC', '2016-08-01', '1993-06-10'),
(104, 'Wolfgang', 'Müller', 'WM', NULL, 'mueller@dc-solution.de', 0, '2', '2', '', '', '', 0, 'de', 0, NULL, NULL, 0, 0, NULL, 0, 0, 200, 0, 64, 'Consultant', '', 'KEY-ACC', '2016-07-01', '1983-09-16');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `team_id` (`team_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=105;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
