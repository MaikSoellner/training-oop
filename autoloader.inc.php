<?php
/**
 * Created by PhpStorm.
 * User: msoellner
 * Date: 08.02.2018
 * Time: 11:11
 */

/**
 * Beim Inkludieren der Klassen muss auf die Reihenfolge geachtet werden.
 * Zuerst werden Interfaces inkludiert, da diese keine anderen Voraussetzungen haben.
 * Danach kommen Traits und zum Schluss alle anderen Klassen, da diese von Interfaces, Traits, etc. abhängig sein können.
 */

date_default_timezone_set('Europe/Berlin');

define('ROOT_DIR_OOP', __DIR__ . DIRECTORY_SEPARATOR . 'common');

define('DIR_CLASSES', ROOT_DIR_OOP . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR);
define('DIR_INTERFACES', ROOT_DIR_OOP . DIRECTORY_SEPARATOR . 'interfaces' . DIRECTORY_SEPARATOR);
define('DIR_TRAITS', ROOT_DIR_OOP . DIRECTORY_SEPARATOR . 'traits' . DIRECTORY_SEPARATOR);


// + + + OOP Classes + + +
getAllInterfaces();
getAllTraits();
getAllClasses();
// - - - OOP Classes - - -


function getAllInterfaces()
{
    $files = array_diff(scandir(DIR_INTERFACES), array('..', '.'));
    foreach($files as $file){
        require_once DIR_INTERFACES . $file;
    }
}

function getAllTraits()
{
    $files = array_diff(scandir(DIR_TRAITS), array('..', '.'));
    foreach($files as $file){
        require_once DIR_TRAITS . $file;
    }
}

function getAllClasses()
{
    $files = array_diff(scandir(DIR_CLASSES), array('..', '.'));
    foreach($files as $file){
        require_once DIR_CLASSES . $file;
    }
}
?>